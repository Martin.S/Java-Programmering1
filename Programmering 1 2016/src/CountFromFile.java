import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class CountFromFile {
	Scanner scan;

	public static void main(String[] args) throws Exception {
		
		System.out.println("Text:");
		System.out.println(fileToString());
		System.out.println("Text info: ");
		System.out.println("Number of characters "+getNumberOfCharsInString(fileToString()));
		System.out.println("Number of non-characters: "+getNumberOfOtherInString(fileToString()));
		System.out.println("Number of words: "+getNumberOfWordsInString(fileToString()));
		System.out.println("Number of Lines: "+getNumberOfLinesInString(fileToString()));


	}

	public static String fileToString() throws Exception {
		String content = new String(Files.readAllBytes(Paths.get("test.txt")));
		return content;

		
	}

	public static int getNumberOfCharsInString(String s) {
		int number = 0;
		for (int i = 0; i<s.length(); i++){
		number = number+1;
		}
		int spaces = s.split("[ \\s]").length;
		int lines = s.split("\\n").length;
		return number-spaces-lines;
	}

	
	public static int getNumberOfWordsInString(String s) {
		int number = s.split("[ \\t\\r]").length;
		return number;
		
	}
	public static int getNumberOfLinesInString(String s) {
		int number = s.split("\\n").length;
		return number;
		
	}
	public static int getNumberOfOtherInString(String s) {
		int number = s.split("^\\w").length;
		return number;
}}
