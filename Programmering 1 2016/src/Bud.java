import java.util.Calendar;
import java.util.GregorianCalendar;

public class Bud {
	private int bidNumber;
	private String bidDate;
	private String bidDeadline;
	private float amount;
	private String navn, number;

	public Bud(float amount, String navn, String number) {
		this.bidNumber = bidNumber + 1;
		this.navn = navn;
		this.number = number;
		this.bidDate = dateOfToday();
	}

	public Bud(float amount, String navn, String number, int day, int month, int year) {
		this.amount = amount;
		this.bidNumber = bidNumber + 1;
		this.navn = navn;
		this.number = number;
		this.bidDate = dateOfToday();
		this.bidDeadline = IntToGC(day ,month, year);
	}

	private String dateOfToday() {
		Calendar dateOfToday = new GregorianCalendar();
		int year = dateOfToday.get(Calendar.YEAR);
		int month = 1 + dateOfToday.get(Calendar.MONTH) + 1;
		int dayOfMonth = dateOfToday.get(Calendar.DAY_OF_MONTH);
		int hourOfDay = dateOfToday.get(Calendar.HOUR_OF_DAY);
		int minute = dateOfToday.get(Calendar.MINUTE);
		String today = dayOfMonth + "/" + month + "/" + year + " " + hourOfDay + ":" + minute;
		return today;

	}

	public String IntToGC(int day, int month, int year){
		Calendar dateOfToday = new GregorianCalendar();
		int yearDeadline = year;
		int monthDeadline = month;
		int dayOfMonthDeadline = day;
		String gCalendar = dayOfMonthDeadline + "/" + monthDeadline + "/" + yearDeadline;
		return gCalendar;
	}
	
	
	public int getBidNumber() {
		return bidNumber;
	}

	public void setBidNumber(int bidNumber) {
		this.bidNumber = bidNumber;
	}

	public String getBidDate() {
		return bidDate;
	}

	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}

	public String getBidDeadline() {
		return bidDeadline;
	}

	public void setBidDeadline(String bidDeadline) {
		this.bidDeadline = bidDeadline;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
