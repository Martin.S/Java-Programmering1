import java.util.Scanner;

public class EiendomTest {
	private static Scanner Scanner;

	public static void main(String[] args) {
		// Oppretter enebolig objekter
		Enebolig eb1 = new Enebolig("Marihandstien 2", "Ole Joan Olsen", 1100000, 204, 23, 400, Enebolig.FRITTSTAAENDE);
		Enebolig eb2 = new Enebolig("Sentrums Gate 49", "Tommy Stien", 1350000, 230, 23, 400, Enebolig.TOMANNS);
		Enebolig eb3 = new Enebolig("Rekkeveien 10", "Anders Andersen", 600000, 70, 23, 400, Enebolig.REKKE);
		
		// Oppretter leilighet objekter
		Leilighet lh1 = new Leilighet("Nordlig Blokk A1", "Hybel A/S", 90000, 20, 25, 400, 30000, 5000, 2);
		
		// Legger inn noen bud
		eb1.newBid("Ole Pedersen", 2400000, "90522355", 24,2,2006);
		eb1.newBid("John McIntosh", 2500000, "90822655", 12,6,2009);
		eb2.newBid("John Doe", 3000000, "97093207", 12,6,2010);
		lh1.newBid("Student 1", 5000, "12356789", 25, 6, 2015);
		lh1.newBid("Student 2", 5000, "25436524", 26, 5, 2015);
		lh1.newBid("Student 3", 5000, "99492634", 20, 6, 2015);
		lh1.newBid("Student 4", 5000, "96340563", 10, 6, 2015);
		
		// Lister alle bud
		//Skift "eb1" til navn p� object i printInfo og printAllBids for � info for korrekt enebolig
		lh1.printInfo();
		lh1.printAllBids();
		
		//Forst�r ikke hva gnr, bnr eller felles gjeld er, dette st�r aldri forklart i oppgaven
		
	}
}
