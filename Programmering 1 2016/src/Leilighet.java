
public class Leilighet extends Eiendom {
	private double sharedTax;
	private double rent;
	private int floor;

	public Leilighet() {

	}

	public Leilighet(String adress, String ownersName, double tax, int antKvm, int gnr, int bnr, double sharedTax, double rent, int floor) {
		super.adress = adress;
		super.ownersName = ownersName;
		super.tax = tax;
		super.antKvm = antKvm;
		super.gnr = gnr;
		super.bnr = bnr;
		this.sharedTax = sharedTax;
		this.rent = rent;
		this.floor = floor;
	}
	
	public void printInfo(){
		Leilighet object = Leilighet.this;
		System.out.printf ("%-20s%-35s\n","Bud gitt for: " ,object.getAdress());
		System.out.printf("%-20s%-35s\n", "Eierens navn: ", object.getOwnersName());
		System.out.printf("%-20s%-35s\n", "Etsje: ", object.getFloor());
		System.out.printf("%-20s%-1s%-1s\n", "Husleie: ", object.getRent(), " kr");
		System.out.printf("%-20s%-1s%-1s\n", "Felles gjeld: ", object.getSharedTax()," kr");
		System.out.printf ("%-20s%-1s%-1s\n","Takst: ",object.getTax()," kr");
		System.out.printf("%-20s%-35s\n\n\n", "Kvaderat meter: ", object.getAntKvm());
		
	}
		
	public void printAllBids() {
		System.out.printf("%-20s%-20s%-20s%-20s%-20s\n", "Bud gitt", "Bud frist", "Navn", "Telefon", "Bel�p");
		for (int i = 0; i < super.BudArray.size(); i++) {
			Bud bud = (Bud) super.BudArray.get(i);
			System.out.printf("%-20s%-20s%-20s%-20s%-1s%-1s\n", bud.getBidDate(), bud.getBidDeadline(), bud.getNavn(),
					bud.getNumber(), bud.getAmount(), " kr");
		}
	}

	public double getSharedTax() {
		return sharedTax;
	}

	public void setSharedTax(double sharedTax) {
		this.sharedTax = sharedTax;
	}

	public double getRent() {
		return rent;
	}

	public void setRent(double rent) {
		this.rent = rent;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}
}