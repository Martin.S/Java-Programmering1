import java.util.Scanner;

public class ArrayIndexOutOfBoundsException {
	private static Scanner scan;

	public static void main(String[] args) {
		scan = new Scanner(System.in);
		int[] arrayOfInt = new int[100];
		
		for (int i = 0; i <= 99; i++) {
			int random = (int) ((Math.random() * 100) + 1);
			arrayOfInt[i] = random;
		}
		System.out.println("Enter index; ");
		int index = scan.nextInt();
		
		try{
		System.out.println(arrayOfInt[index]);
		}catch (Exception ex){
			System.out.println("This index is out of bounds");
		}
	}
}