import java.util.ArrayList;

public class AccountTest {

	public static void main(String[] args) {
		NewAccountClass account1 = new NewAccountClass("Martin", 200, 1.5, 12);
		account1.deposit(200);
		account1.withdraw(30);
		account1.withdraw(60);
		account1.deposit(122);
		account1.withdraw(92);
		account1.deposit(12);
		account1.withdraw(65);

		System.out.printf("%-25s%s\n%-25s%s\n%-25s%s\n%-25s%s\n%-25s%s\n\n\n", "Name:", account1.getCustomerName(), "ID:",account1.getId(), "Created:", 
				account1.getDate(), "Montly Interest Rate:" ,account1.getMonthlyInterest(), "Balance:", account1.getBalance());

		ArrayList history = account1.getTransactions();
		System.out.printf("%-35s%-20s%-20s%-20s\n", "Transaction date", "Type", "Amount", "Account balance");

		for (int i = 0; i < history.size(); i++) {
			Transaction trans = (Transaction) history.get(i);
			if (trans.getTransactiontype() == 'D') {
				System.out.printf("%-35s%-20s%-20s%-20s\n", trans.getTransactionDate(), "Deposit",
						trans.getTransactionAmount(), trans.getAccountBalance());
			} else {
				System.out.printf("%-35s%-20s%-20s%-20s\n", trans.getTransactionDate(), "Withdrawal",
						trans.getTransactionAmount(), trans.getAccountBalance());
			}

		}

	} 

}
