import java.util.Date;

public class Transaction {

	private Date transactionDate = new Date();
	private char transactiontype;
	private double transactionAmount;
	private double accountBalance;
	private String description = new String();

	public Transaction(char transactiontype, double transactionAmount, double accountBalance, String description) {
		transactionDate = new Date();
		this.transactiontype = transactiontype;
		this.transactionAmount = transactionAmount;
		this.accountBalance = accountBalance;
		this.description = description;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public char getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(char transactiontype) {
		this.transactiontype = transactiontype;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
};
