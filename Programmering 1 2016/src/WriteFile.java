
public class WriteFile {
	
	public static void main(String[] args) throws Exception {
		java.io.File file = new java.io.File("test.txt");
		if(file.exists()){
			System.out.println("File already exists");
			System.exit(0);
		}
		java.io.PrintWriter output = new java.io.PrintWriter(file);
		
		output.println ("this is a test document");
		output.println ("nothing to see on line 2");
		output.println ("why are you still reading this?");
		output.close();
	}
}
