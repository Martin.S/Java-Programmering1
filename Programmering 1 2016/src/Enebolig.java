
public class Enebolig extends Eiendom {
	final static int FRITTSTAAENDE = 1;
	final static int REKKE = 2;
	final static int TOMANNS = 3;
	private int houseType;
	
	public Enebolig(int input){
		this.houseType = input;
	}
	

	public Enebolig(String adress, String ownersName, double tax, int antKvm, int gnr, int bnr,int input){
		super.adress = adress;
		super.ownersName = ownersName;
		super.tax = tax;
		super.antKvm = antKvm;
		super.gnr = gnr;
		super.bnr = bnr;
		this.houseType = input;
	}

	public void printInfo(){
		Enebolig object = Enebolig.this;
		System.out.printf ("%-20s%-35s\n","Bud gitt for: " ,object.getAdress());
		System.out.printf("%-20s%-35s\n", "Eierens navn: ", object.getOwnersName());
		System.out.printf("%-20s%-35s\n", "Enebolig type: ", object.getTypeAsString());
		System.out.printf ("%-20s%-1s%-1s\n","Takst: ",object.getTax()," kr");
		System.out.printf("%-20s%-35s\n\n\n", "Kvaderat meter: ", object.getAntKvm());
		
	}
		
	public void printAllBids() {
		System.out.printf("%-20s%-20s%-20s%-20s%-20s\n", "Bud gitt", "Bud frist", "Navn", "Telefon", "Bel�p");
		for (int i = 0; i < super.BudArray.size(); i++) {
			Bud bud = (Bud) super.BudArray.get(i);
			System.out.printf("%-20s%-20s%-20s%-20s%-1s%-1s\n", bud.getBidDate(), bud.getBidDeadline(), bud.getNavn(),
					bud.getNumber(), bud.getAmount(), " kr");
		}
	}

	public String getTypeAsString() {
		if (houseType==1){
			return "FRITTSTAAENDE";
		}
		if (houseType==2){
			return "REKKE";
		}
		if (houseType==3){
			return "TOMANNS";
		}else{
			return "";
		}
		
	}

	public int getTypeAsInt(String type) {
		int returnThis = 0;
		switch (type) {
		case "FRITTSTAAENDE":
			returnThis = 1;
		case "REKKE":
			returnThis = 2;
		case "TOMANNS":
			returnThis = 3;
		}
		return returnThis;

	}


	public int getHouseType() {
		return houseType;
	}


	public void setHouseType(int houseType) {
		this.houseType = houseType;
	}
	
}
