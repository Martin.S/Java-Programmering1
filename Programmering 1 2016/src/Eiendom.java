import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Eiendom {
	protected String adress;
	protected String ownersName;
	protected double tax;
	protected int antKvm;
	protected int gnr;
	protected int bnr;
	ArrayList<Bud> BudArray = new ArrayList();

	public Eiendom() {
		this.adress = "N/A";
		this.ownersName = "N/A";
		this.tax = 0;
		this.antKvm = 0;
		this.gnr = 0;
		this.bnr = 0;
	}

	public Eiendom(String adress, String ownersName, double tax, int antKvm, int gnr, int bnr) {
		this.adress = adress;
		this.ownersName = ownersName;
		this.tax = tax;
		this.antKvm = antKvm;
		this.gnr = gnr;
		this.bnr = bnr;
	}

	public void printAllBids(){
		System.out.printf("%-35s%-35s%-20s%-20s-20s%\n", "Bud gitt", "Bud frist", "Navn", "Telefon", "Bel�p");
		for (int i = 0; i < BudArray.size(); i++) {
			Bud bud = (Bud) BudArray.get(i);
			System.out.printf("%-35s%-35s%-20s%-20s%-20s%\n", bud.getBidDate(), bud.getBidDeadline(),
					bud.getNavn(), bud.getNumber(), bud.getAmount());
		}
	}
	public void newBid(String navn, float amount, String number, int day, int month, int year){
		BudArray.add (new Bud(amount, navn, number, day, month, year));
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getOwnersName() {
		return ownersName;
	}

	public void setOwnersName(String ownersName) {
		this.ownersName = ownersName;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public int getAntKvm() {
		return antKvm;
	}

	public void setAntKvm(int antKvm) {
		this.antKvm = antKvm;
	}

	public int getGnr() {
		return gnr;
	}

	public void setGnr(int gnr) {
		this.gnr = gnr;
	}

	public int getBnr() {
		return bnr;
	}

	public void setBnr(int bnr) {
		this.bnr = bnr;
	}


}
