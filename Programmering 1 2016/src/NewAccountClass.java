import java.util.ArrayList;
import java.util.Date;

public class NewAccountClass {

	private int id = 0;
	private double balance = 0, annualInterestRate = 0;
	private Date dateCreated = new Date();
	private String customerName = new String();
	ArrayList<Transaction> transactions = new ArrayList();

	// constructor no args
	public NewAccountClass() {
		balance = 0;
		annualInterestRate = 4.5;
		id++;
		dateCreated = new Date();
	}

	// constructor
	public NewAccountClass(double newBalance, double newAnnualInterestRate, int newId) {
		// date=getDate;
		balance = newBalance;
		annualInterestRate = newAnnualInterestRate;
		id = newId;
		dateCreated = new Date();
	}

	// constructor with Name
	public NewAccountClass(String name, double newBalance, double newAnnualInterestRate, int newId) {
		// date=getDate;
		customerName = name;
		balance = newBalance;
		annualInterestRate = newAnnualInterestRate;
		id = newId;
		dateCreated = new Date();
	}

	// set balance
	public void setBalance(double newBalance) {
		balance = (newBalance >= 0) ? newBalance : 0;
	}

	// return id
	public int getId() {
		return id;
	}

	// return balance
	public double getBalance() {

		return balance;
	}

	// Return date
	public Date getDate() {
		return dateCreated;

	}

	// withdraw
	public void withdraw(double newValue) {
		balance = (balance - newValue >= 0 && newValue > 0) ? balance - newValue : balance;
		transactions.add(new Transaction('W', newValue, balance, ""));
	}

	// deposit
	public void deposit(double newValue) {
		balance = (balance + newValue >= 0 && newValue > 0) ? balance + newValue : balance;
		transactions.add(new Transaction('D', newValue, balance, ""));
	}

	// Monthly interest rate
	public double monthlyInterest() {
		double monthlyInterest = annualInterestRate / 12;
		return monthlyInterest;
	}

	// Return monthly interest
	public double getMonthlyInterest() {
		Double returnMonthlyInterest = balance * monthlyInterest();
		return returnMonthlyInterest;
	}

	// return monthly interest rate

	// Return annual interest rate
	public double annualInterest() {
		double returnAnnualInterest = annualInterestRate * balance;
		return returnAnnualInterest;	
	}
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<Transaction> transactions) {
		this.transactions = transactions;
	}
}

